package com.richardthomas.moodminer2;

import android.provider.BaseColumns;

/**
 * Created by Richard Thomas on 19-Apr-16.
 */
public class DATA_TABLE {
    public DATA_TABLE(){

    }
    public static abstract class TableInfo implements BaseColumns{
        public static final String USER_NAME="user_name";
        public static final String X="x";
        public static final String Y="y";
        public static final String Z="z";

        public static final String LAT="latitude";
        public static final String LON="longitude";

        public static final String DATETIME="date_time";
        public static final String DATABASE_NAME="moodminer";
        public static final String ACCEL_READINGS="accel_readings";
        public static final String GPS_READINGS="gps_readings";
    }
}
