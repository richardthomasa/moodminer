package com.richardthomas.moodminer2;

import com.richardthomas.moodminer2.DATA_TABLE.TableInfo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Richard Thomas on 19-Apr-16.
 */
public class DBoperations extends SQLiteOpenHelper {

    public static final int database_version=2;
    public String CREATE_QUERY_ACCEL = "CREATE TABLE "+TableInfo.ACCEL_READINGS+"("+TableInfo.X+" FLOAT,"+TableInfo.Y+" FLOAT,"+TableInfo.Z+" FLOAT);";
    public String CREATE_QUERY_GPS ="CREATE TABLE "+TableInfo.GPS_READINGS+"("+TableInfo.LAT+" FLOAT,"+TableInfo.LON+"FLOAT"+");";
    public DBoperations(Context context) {
        super(context, TableInfo.DATABASE_NAME, null, database_version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //create all the tables here
        db.execSQL(CREATE_QUERY_ACCEL);
        db.execSQL(CREATE_QUERY_GPS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //upgrade all the tables here
    }
    public void insertAccel(DBoperations dop, float x, float y, float z){
        Log.d("insertAccel:", "inside insertAccel ");
        SQLiteDatabase SQ=dop.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TableInfo.X,x);
        cv.put(TableInfo.Y,y);
        cv.put(TableInfo.Z,z);
        Long k=SQ.insert(TableInfo.ACCEL_READINGS,null,cv);
    }
    public void insertGps(DBoperations dop, float lat, float lon){
        SQLiteDatabase SQ=dop.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TableInfo.LAT,lat);
        cv.put(TableInfo.LON,lon);
        Long k=SQ.insert(TableInfo.GPS_READINGS,null,cv);
    }
    public Cursor fetchAccel(DBoperations dop){
        SQLiteDatabase SQ =dop.getReadableDatabase();
        String[] columns={TableInfo.X,TableInfo.Y,TableInfo.Z};
        Cursor CR=SQ.query(TableInfo.ACCEL_READINGS, columns, null, null, null, null, null);
        return CR;
    }
    public Cursor fetchGps(DBoperations dop){
        SQLiteDatabase SQ =dop.getReadableDatabase();
        String[] columns={TableInfo.LAT,TableInfo.LON};
        Cursor CR=SQ.query(TableInfo.GPS_READINGS, columns, null, null,null, null, null);
        return CR;
    }
    public void delAccel(DBoperations dop){
        SQLiteDatabase SQ =dop.getWritableDatabase();
        SQ.execSQL("delete from "+TableInfo.ACCEL_READINGS);
    }
}
