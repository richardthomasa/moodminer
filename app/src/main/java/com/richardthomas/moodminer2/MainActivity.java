package com.richardthomas.moodminer2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.richardthomas.moodminer2.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE ="com.richardthomas.moodminer.MESSAGE";
    Button loginButton;
    EditText username,password;
    String msg;

    private ProgressDialog pDialog;

    // URL to get contacts JSON
    private static String url = "http://10.0.2.2/moodminer/loginapi.php";
    //private static String url = "http://moodminer1.byethost13.com/loginapi.php";

    // JSON Node names
    private static final String TAG_CONTACTS = "contacts";
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "name";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_ADDRESS = "address";
    private static final String TAG_GENDER = "gender";
    private static final String TAG_PHONE = "phone";
    private static final String TAG_PHONE_MOBILE = "mobile";
    private static final String TAG_PHONE_HOME = "home";
    private static final String TAG_PHONE_OFFICE = "office";
    private static final String TAG_REPLY = "reply";

    // contacts JSONArray
    String reply = null;
    JSONObject c =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginButton=(Button)findViewById(R.id.button);

        username= (EditText) findViewById(R.id.editText);
        password= (EditText) findViewById(R.id.editText2);
        //String msg;

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(username.getText().toString().equals("root")&&
                        password.getText().toString().equals("root")){
                    //if_body
                    //if the user is valid
                    Toast.makeText(MainActivity.this, "Login successful...", Toast.LENGTH_SHORT).show();
                    Intent i= new Intent(MainActivity.this,dashboard.class);
                    //i.putExtra(EXTRA_MESSAGE,"root");

                    Log.e("mady", "reached here" );
                    startActivity(i);
                }
                else{
                    Toast.makeText(MainActivity.this, "Login unsuccessful.Try again...", Toast.LENGTH_SHORT).show();
                }

                /*String uname=username.getText().toString();
                String pword=password.getText().toString();

                List<NameValuePair> nm = new ArrayList<NameValuePair>();
                nm.add(new BasicNameValuePair("username",uname));
                nm.add(new BasicNameValuePair("password",pword));

                new GetContacts().execute(nm);*/

            }
        });

    }
    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetContacts extends AsyncTask<List<NameValuePair>, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(List<NameValuePair>... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST,arg0[0]);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    reply = jsonObj.getString(TAG_REPLY);
                    //c = reply.getJSONObject(0);

                    /* looping through All Contacts
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);

                        String id = c.getString(TAG_ID);
                        String name = c.getString(TAG_NAME);
                        String email = c.getString(TAG_EMAIL);
                        String address = c.getString(TAG_ADDRESS);
                        String gender = c.getString(TAG_GENDER);

                        // Phone node is JSON Object
                        JSONObject phone = c.getJSONObject(TAG_PHONE);
                        String mobile = phone.getString(TAG_PHONE_MOBILE);
                        String home = phone.getString(TAG_PHONE_HOME);
                        String office = phone.getString(TAG_PHONE_OFFICE);


                    }*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             *
            ListAdapter adapter = new SimpleAdapter(
                    MainActivity.this, contactList,
                    R.layout.list_item, new String[] { TAG_NAME, TAG_EMAIL,
                    TAG_PHONE_MOBILE }, new int[] { R.id.name,
                    R.id.email, R.id.mobile });

            setListAdapter(adapter);*/
            //try {
                //if(c.getString(TAG_REPLY)=="success")
            try {
                if (reply.equals("success")) {
                    Toast.makeText(MainActivity.this, "Login Success", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(MainActivity.this, dashboard.class);
                    startActivity(i);
                }
                //else if(c.getString(TAG_REPLY)=="failed"){
                else if (reply.equals("failed")) {
                    Toast.makeText(MainActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(MainActivity.this, "Some error occured...", Toast.LENGTH_SHORT).show();
                }
                //}
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

    }
}
