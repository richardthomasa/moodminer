package com.richardthomas.moodminer2;


import com.richardthomas.moodminer2.ServiceHandler;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

//import com.richardthomas.moodminer.moodminer.GPSReader;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class dashboard extends AppCompatActivity implements SensorEventListener{

    String uploadUrl ="http://192.168.43.244/moodminer/uploadpage.php";
    String moodurl ="http://192.168.0.25/moodminer/parse.php";
    private static final String TAG_REPLY = "reply";
    private static final String TAG = "Debug";
    boolean flag =true;
    SensorManager sensorManager;
    Sensor accelerometer;
    private float last_x,last_y,last_z;
    private long lastupdate;
    private static final int SHAKE_THRESHOLD=600;
    TextView txt_acc,txt_call ;
    Button uploadBtn;
    protected LocationManager locationManager;
    DBoperations dob = new DBoperations(this);
    Geocoder gcd;

    public static Location location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        Intent intent=getIntent();
        //String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        //startService(new Intent(this,dataCollectionService.class));
        sensorManager= (SensorManager) getSystemService(Context.SENSOR_SERVICE);//get a sensor manager which contains the list of all sensors
        accelerometer=sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);//get accelerometer sensor from sensor manager
        sensorManager.registerListener(this,accelerometer,sensorManager.SENSOR_DELAY_NORMAL);//listen to the device's accelerometer data

        uploadBtn = (Button) findViewById(R.id.button2);
        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new dbUploadOperation().execute(null,null,null);
            }
        });
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener ll = new mylocationlistener();


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 10, ll);


        txt_acc= (TextView) findViewById(R.id.textView9);//for displaying accelerometer readings

        //callRecord cr =new callRecord();
        //StringBuffer sb =cr.getCallDetails();
        //txt_call = (TextView) findViewById(R.id.textView10);
        //txt_call.setText(sb.toString());

        //     LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //     LocationListener ll = new mylocationlistener();


        //noinspection ResourceType
        //     lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,5000, 10, ll);


        // locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // LocationListener l1=new mylocationlistner();

        //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,30000,10,l1);

        /**
         GPSReader gReader =new GPSReader();
         String s = gReader.getGPSReading();
         Toast.makeText(dashboard.this, s, Toast.LENGTH_SHORT).show();
         **/
    }
    public class moodFetch extends AsyncTask<String,String,String>{

        @Override
        protected String doInBackground(String... params) {
            ServiceHandler sh = new ServiceHandler();
            String jsonresp =sh.makeServiceCall(moodurl,ServiceHandler.POST);
            Log.d("Response: ", "> " + jsonresp);

            if(jsonresp !=null){
                try {
                    JSONObject jsonObj = new JSONObject(jsonresp);
                    jsonObj.getString("tiredness");
                    jsonObj.getString("tensity");
                    jsonObj.getString("displeasure");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                Log.d("error: ", "Error fetching mood ");
            }
            return null;
        }
    }

    public class dbUploadOperation extends AsyncTask<String,String,String>{

        String reply = new String();
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Cursor cr= dob.fetchAccel(dob);
            startManagingCursor(cr);
            if(cr.moveToFirst()){
                do {
                    float db_x= cr.getFloat(cr.getColumnIndex(DATA_TABLE.TableInfo.X));
                    Log.d("x value:",String.valueOf(db_x));
                    float db_y= cr.getFloat(cr.getColumnIndex(DATA_TABLE.TableInfo.Y));
                    Log.d("y value:",String.valueOf(db_y));
                    float db_z= cr.getFloat(cr.getColumnIndex(DATA_TABLE.TableInfo.Z));
                    Log.d("z value:",String.valueOf(db_z));
                    List<NameValuePair> nm = new ArrayList<NameValuePair>();
                    nm.add(new BasicNameValuePair("x",String.valueOf(db_x)));
                    nm.add(new BasicNameValuePair("y",String.valueOf(db_y)));
                    nm.add(new BasicNameValuePair("z", String.valueOf(db_z)));
                    ServiceHandler sh = new ServiceHandler();
                    String jsonresp =sh.makeServiceCall(uploadUrl,ServiceHandler.POST,nm);
                    Log.d("Response: ", "> " + jsonresp);

                    if (jsonresp != null) {
                        try {
                            JSONObject jsonObj = new JSONObject(jsonresp);

                            // Getting JSON Array node
                            reply = jsonObj.getString(TAG_REPLY);
                            if(reply.equals("success")){
                                flag=true;
                            }
                            else{
                                flag=false;
                                break;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }while (cr.moveToNext());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            if(flag==false){
                Toast.makeText(dashboard.this, "upload success", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(dashboard.this, "upload failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class mylocationlistener implements LocationListener {
        double lat,lon;
        String cityName;

        public void onLocationChanged(Location location) {
            Log.e("innnnnnnnnn", "innnnnn");
            // TODO Auto-generated method stub

            if (location != null) {

                Toast.makeText(getBaseContext(),"Location changed : Lat: " +
                                location.getLatitude()+ " Lng: " + location.getLongitude(),
                        Toast.LENGTH_SHORT).show();

                String longitude = "Longitude: " +location.getLongitude();
                Log.v(TAG, longitude);
                String latitude = "Latitude: " +location.getLatitude();
                Log.v(TAG, latitude);
                Log.d("LOCATION CHANGED", location.getLatitude() + "");
                Log.d("LOCATION CHANGED", location.getLongitude() + "");
                //

                //shared preferences
                String latshare,longshare;
                latshare=String.valueOf(location.getLatitude());
                longshare=String.valueOf(location.getLongitude());

                //
				        /*----------to get City-Name from coordinates ------------- */
                cityName=null;
                String LocalityName=null;
                String subloc=null;
                gcd = new Geocoder(getBaseContext(), Locale.ENGLISH);

                try {
                    List<Address> addresses;
                    Log.e("inside try", "Ddd");
                    addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    if (addresses.size() > 0)
                        System.out.println(addresses.get(0).getLocality());
                    cityName=addresses.get(0).getLocality();
                    LocalityName = addresses.get(0).getAddressLine(0);
                    subloc=addresses.get(0).getSubLocality();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                String  s = longitude+"\n"+latitude +
                        "\n\nMy Currrent City is: "+cityName+"\nLocalityName:"+LocalityName+"\nSubLoc:"+subloc;
                Toast.makeText(getApplicationContext(),"k"+s,Toast.LENGTH_SHORT).show();


            }

        }


        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub
        }

        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub
        }

    }



    @Override
    public void onSensorChanged(SensorEvent event) {

        Sensor mysensor=event.sensor;

        if (mysensor.getType() == Sensor.TYPE_ACCELEROMETER){
            float x =event.values[0];
            float y =event.values[1];
            float z =event.values[2];

            long curtime=System.currentTimeMillis();
            if((curtime-lastupdate)>100){
                long difftime=(curtime-lastupdate);
                lastupdate =curtime;

                float speed=Math.abs(x+y+z -last_x -last_y -last_z)/difftime*10000;

                if (speed>SHAKE_THRESHOLD) {


                    last_x = x;
                    last_y = y;
                    last_z = z;

                    try {
                        DBoperations dop = new DBoperations(this);
                        dop.insertAccel(dop, last_x, last_y, last_z);
                        Cursor cr =null;
                        cr=dop.fetchAccel(dop);
                        if(cr!=null)
                        {
                            Log.d("Db Accel: ", "Accel data inserted");
                            Toast.makeText(dashboard.this, "Successfully inserted to db", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Log.d("Db Accel: ", "Accel data insertion failed");
                            Toast.makeText(dashboard.this, "db insertion failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }



                    txt_acc.setText("x:" + last_x + ",y:" + last_y + ",z:" + last_z);
                }

            }
        }
    }

    @Override
    public void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy) {

    }
}
