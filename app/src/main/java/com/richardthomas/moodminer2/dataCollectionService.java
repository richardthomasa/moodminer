package com.richardthomas.moodminer2;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Richard Thomas on 01-Mar-16.
 */
public class dataCollectionService extends Service implements SensorEventListener {

    SensorManager sensorManager;
    Sensor accelerometer;
    private float last_x,last_y,last_z;
    private long lastupdate;
    private static final int SHAKE_THRESHOLD=600;

    @Override
    public IBinder onBind(Intent arg0) {

        return null;
    }

    @Override
    public void onStart(Intent intent,int startId){

        sensorManager= (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer=sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener((SensorEventListener) this,accelerometer,sensorManager.SENSOR_DELAY_NORMAL);
        super.onStart(intent, startId);
    }

    @Override
    public void onCreate(){
        super.onCreate();
        sensorManager= (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer=sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener((SensorEventListener) this, accelerometer, sensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor mysensor=event.sensor;

        if (mysensor.getType() == Sensor.TYPE_ACCELEROMETER){
            float x =event.values[0];
            float y =event.values[1];
            float z =event.values[2];

            long curtime=System.currentTimeMillis();
            if((curtime-lastupdate)>100){
                long difftime=(curtime-lastupdate);
                lastupdate =curtime;

                float speed=Math.abs(x+y+z -last_x -last_y -last_z)/difftime*10000;

                if (speed>SHAKE_THRESHOLD) {


                    last_x = x;
                    last_y = y;
                    last_z = z;



                }

            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
